/*
 * This file is part of the Serial Flash Universal Driver Library.
 *
 * Copyright (c) 2016-2018, Armink, <armink.ztl@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * 'Software'), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Function: Portable interface for each platform.
 * Created on: 2016-04-23
 */

#include <sfud.h>
#include <stdarg.h>
#include <rtthread.h>

#include "spi.h"
#include "gpio.h"

typedef struct {
    SPI_HandleTypeDef *spix;
    GPIO_TypeDef *cs_gpiox;
    uint16_t cs_gpio_pin;
} spi_user_data, *spi_user_data_t;

#ifdef BSP_USING_SPI1
extern SPI_HandleTypeDef hspi1;
static spi_user_data user_spi = { .spix = &hspi1, .cs_gpiox = BSP_DATAFALSH_CS_GPIOX, .cs_gpio_pin = BSP_DATAFALSH_CS_GPIO_PIN };
#define MX_SPI_Init()	MX_SPI1_Init()
#endif
#ifdef BSP_USING_SPI2
extern SPI_HandleTypeDef hspi2;
static spi_user_data user_spi = { .spix = &hspi2, .cs_gpiox = BSP_DATAFALSH_CS_GPIOX, .cs_gpio_pin = BSP_DATAFALSH_CS_GPIO_PIN };
#define MX_SPI_Init()	MX_SPI2_Init()
#endif
#ifdef BSP_USING_SPI3
extern SPI_HandleTypeDef hspi3;
static spi_user_data user_spi = { .spix = &hspi3, .cs_gpiox = BSP_DATAFALSH_CS_GPIOX, .cs_gpio_pin = BSP_DATAFALSH_CS_GPIO_PIN };
#define MX_SPI_Init()	MX_SPI3_Init()
#endif
#ifdef BSP_USING_SPI4
extern SPI_HandleTypeDef hspi4;
static spi_user_data user_spi = { .spix = &hspi4, .cs_gpiox = BSP_DATAFALSH_CS_GPIOX, .cs_gpio_pin = BSP_DATAFALSH_CS_GPIO_PIN };
#define MX_SPI_Init()	MX_SPI4_Init()
#endif
#ifdef BSP_USING_SPI5
extern SPI_HandleTypeDef hspi5;
static spi_user_data user_spi = { .spix = &hspi5, .cs_gpiox = BSP_DATAFALSH_CS_GPIOX, .cs_gpio_pin = BSP_DATAFALSH_CS_GPIO_PIN };
#define MX_SPI_Init()	MX_SPI5_Init()
#endif
#ifdef BSP_USING_SPI6
extern SPI_HandleTypeDef hspi6;
static spi_user_data user_spi = { .spix = &hspi6, .cs_gpiox = BSP_DATAFALSH_CS_GPIOX, .cs_gpio_pin = BSP_DATAFALSH_CS_GPIO_PIN };
#define MX_SPI_Init()	MX_SPI6_Init()
#endif

static struct rt_mutex                 lock;
static char log_buf[256];



static void spi_lock(const sfud_spi *spi) {
    sfud_flash *sfud_dev = (sfud_flash *) (spi->user_data);
    struct spi_flash_device *rtt_dev = (struct spi_flash_device *) (sfud_dev->user_data);

    RT_ASSERT(spi);
    RT_ASSERT(sfud_dev);
    RT_ASSERT(rtt_dev);

    rt_mutex_take(&lock, RT_WAITING_FOREVER);
}

static void spi_unlock(const sfud_spi *spi) {
    sfud_flash *sfud_dev = (sfud_flash *) (spi->user_data);
    struct spi_flash_device *rtt_dev = (struct spi_flash_device *) (sfud_dev->user_data);

    RT_ASSERT(spi);
    RT_ASSERT(sfud_dev);
    RT_ASSERT(rtt_dev);

    rt_mutex_release(&lock);
}

static void retry_delay_100us(void) {
    /* 100 microsecond delay */
    rt_thread_delay((RT_TICK_PER_SECOND * 1 + 9999) / 10000);
}
void sfud_log_debug(const char *file, const long line, const char *format, ...);

/**
 * SPI write data then read data
 */
static sfud_err spi_write_read(const sfud_spi *spi, const uint8_t *write_buf, size_t write_size, uint8_t *read_buf,
        size_t read_size) {
    sfud_err result = SFUD_SUCCESS;
    spi_user_data_t spi_dev = (spi_user_data_t) spi->user_data;
    /**
     * add your spi write and read code
     */
    RT_ASSERT(spi);
	
	HAL_GPIO_WritePin(spi_dev->cs_gpiox, spi_dev->cs_gpio_pin, GPIO_PIN_RESET);
	if(write_size && read_size)
	{
		if(HAL_SPI_Transmit(spi_dev->spix, (uint8_t *)write_buf, write_size, 1000)!=HAL_OK)	
		{
			result = SFUD_ERR_WRITE;
		}	
	   /* For simplicity reasons, this example is just waiting till the end of the
	   transfer, but application may perform other tasks while transfer operation
	   is ongoing. */
        while (HAL_SPI_GetState(spi_dev->spix) != HAL_SPI_STATE_READY);
		if(HAL_SPI_Receive(spi_dev->spix, (uint8_t *)read_buf, read_size, 1000)!=HAL_OK)
		{
			result = SFUD_ERR_READ;
		}
	}else if(write_size)
	{
		if(HAL_SPI_Transmit(spi_dev->spix, (uint8_t *)write_buf, write_size, 1000)!=HAL_OK)	
		{
			result = SFUD_ERR_WRITE;
		}
	}else
	{
		if(HAL_SPI_Receive(spi_dev->spix, (uint8_t *)read_buf, read_size, 1000)!=HAL_OK)
		{
			result = SFUD_ERR_READ;
		}
	}
	   /* For simplicity reasons, this example is just waiting till the end of the
    transfer, but application may perform other tasks while transfer operation
    is ongoing. */
	while (HAL_SPI_GetState(spi_dev->spix) != HAL_SPI_STATE_READY);
	HAL_GPIO_WritePin(spi_dev->cs_gpiox, spi_dev->cs_gpio_pin, GPIO_PIN_SET);
    return result;
}

#ifdef SFUD_USING_QSPI
/**
 * read flash data by QSPI
 */
static sfud_err qspi_read(const struct __sfud_spi *spi, uint32_t addr, sfud_qspi_read_cmd_format *qspi_read_cmd_format,
        uint8_t *read_buf, size_t read_size) {
    sfud_err result = SFUD_SUCCESS;

    /**
     * add your qspi read flash data code
     */
    RT_ASSERT(spi);
    RT_ASSERT(sfud_dev);
    RT_ASSERT(rtt_dev);

			
    return result;
}
#endif /* SFUD_USING_QSPI */

sfud_err sfud_spi_port_init(sfud_flash *flash) {
    sfud_err result = SFUD_SUCCESS;

    /**
     * add your port spi bus and device object initialize code like this:
     * 1. rcc initialize
     * 2. gpio initialize
     * 3. spi device initialize
     * 4. flash->spi and flash->retry item initialize
     *    flash->spi.wr = spi_write_read; //Required
     *    flash->spi.qspi_read = qspi_read; //Required when QSPI mode enable
     *    flash->spi.lock = spi_lock;
     *    flash->spi.unlock = spi_unlock;
     *    flash->spi.user_data = &spix;
     *    flash->retry.delay = null;
     *    flash->retry.times = 10000; //Required
     */
	rt_mutex_init(&lock, "sfud_lock", RT_IPC_FLAG_FIFO);	
	MX_SPI_Init();
	#if defined(SOC_SERIES_STM32L4) || defined(SOC_SERIES_STM32F0) \
        || defined(SOC_SERIES_STM32F7) || defined(SOC_SERIES_STM32G0)
	
    SET_BIT(hspi2.Instance->CR2, SPI_RXFIFO_THRESHOLD_HF);
	#endif
	switch (flash->index) {
	case SFUD_W25QXX_DEVICE_INDEX: {

			/* 同步 Flash 移植所需的接口及数据 */
			flash->spi.wr = spi_write_read;
			flash->spi.lock = spi_lock;
			flash->spi.unlock = spi_unlock;
			flash->spi.user_data = &user_spi;
			/* about 100 microsecond delay */
			flash->retry.delay = retry_delay_100us;
			/* adout 60 seconds timeout */
			flash->retry.times = 60 * 10000;

			break;
		}
    }
    return result;
}

/**
 * This function is print debug info.
 *
 * @param file the file which has call this function
 * @param line the line number which has call this function
 * @param format output format
 * @param ... args
 */
void sfud_log_debug(const char *file, const long line, const char *format, ...) {
    va_list args;

    /* args point to the first variable parameter */
    va_start(args, format);
    rt_kprintf("[SFUD](%s:%ld) ", file, line);
    /* must use vprintf to print */
    rt_vsnprintf(log_buf, sizeof(log_buf), format, args);
    rt_kprintf("%s\n", log_buf);
    va_end(args);
}

/**
 * This function is print routine info.
 *
 * @param format output format
 * @param ... args
 */
void sfud_log_info(const char *format, ...) {
    va_list args;

    /* args point to the first variable parameter */
    va_start(args, format);
    rt_kprintf("[SFUD]");
    /* must use vprintf to print */
    rt_vsnprintf(log_buf, sizeof(log_buf), format, args);
    rt_kprintf("%s\n", log_buf);
    va_end(args);
}

#if defined(RT_USING_FINSH) && defined(FINSH_USING_MSH)

#include <finsh.h>

static void sf(uint8_t argc, char **argv) {

#define __is_print(ch)                ((unsigned int)((ch) - ' ') < 127u - ' ')
#define HEXDUMP_WIDTH                 16
#define CMD_PROBE_INDEX               0
#define CMD_READ_INDEX                1
#define CMD_WRITE_INDEX               2
#define CMD_ERASE_INDEX               3
#define CMD_RW_STATUS_INDEX           4
#define CMD_BENCH_INDEX               5

    sfud_err result = SFUD_SUCCESS;
    static const sfud_flash *sfud_dev = NULL;

    size_t i = 0, j = 0;

    const char* sf_help_info[] = {
            [CMD_PROBE_INDEX]     = "sf probe 			             - probe and init SPI flash",
            [CMD_READ_INDEX]      = "sf read addr size               - read 'size' bytes starting at 'addr'",
            [CMD_WRITE_INDEX]     = "sf write addr data1 ... dataN   - write some bytes 'data' to flash starting at 'addr'",
            [CMD_ERASE_INDEX]     = "sf erase addr size              - erase 'size' bytes starting at 'addr'",
            [CMD_RW_STATUS_INDEX] = "sf status [<volatile> <status>] - read or write '1:volatile|0:non-volatile' 'status'",
            [CMD_BENCH_INDEX]     = "sf bench                        - full chip benchmark. DANGER: It will erase full chip!",
    };

    if (argc < 2) {
        rt_kprintf("Usage:\n");
        for (i = 0; i < sizeof(sf_help_info) / sizeof(char*); i++) {
            rt_kprintf("%s\n", sf_help_info[i]);
        }
        rt_kprintf("\n");
    } else {
        const char *operator = argv[1];
        uint32_t addr, size;

        if (!strcmp(operator, "probe")) {
            if (argc < 2) {
                rt_kprintf("Usage: %s.\n", sf_help_info[CMD_PROBE_INDEX]);
            } else {

                sfud_dev = sfud_get_device(SFUD_W25QXX_DEVICE_INDEX);;
                if (sfud_dev->chip.capacity < 1024 * 1024) {
                    rt_kprintf("%d KB %s is current selected device.\n", sfud_dev->chip.capacity / 1024, sfud_dev->name);
                } else {
                    rt_kprintf("%d MB %s is current selected device.\n", sfud_dev->chip.capacity / 1024 / 1024,
                            sfud_dev->name);
                }
            }
        } else {
            if (!sfud_dev) {
                rt_kprintf("No flash device selected. Please run 'sf probe'.\n");
                return;
            }
            if (!rt_strcmp(operator, "read")) {
                if (argc < 4) {
                    rt_kprintf("Usage: %s.\n", sf_help_info[CMD_READ_INDEX]);
                    return;
                } else {
                    addr = strtol(argv[2], NULL, 0);
                    size = strtol(argv[3], NULL, 0);
                    uint8_t *data = rt_malloc(size);
                    if (data) {
                        result = sfud_read(sfud_dev, addr, size, data);
                        if (result == SFUD_SUCCESS) {
                            rt_kprintf("Read the %s flash data success. Start from 0x%08X, size is %ld. The data is:\n",
                                    sfud_dev->name, addr, size);
                            rt_kprintf("Offset (h) 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F\n");
                            for (i = 0; i < size; i += HEXDUMP_WIDTH)
                            {
                                rt_kprintf("[%08X] ", addr + i);
                                /* dump hex */
                                for (j = 0; j < HEXDUMP_WIDTH; j++) {
                                    if (i + j < size) {
                                        rt_kprintf("%02X ", data[i + j]);
                                    } else {
                                        rt_kprintf("   ");
                                    }
                                }
                                /* dump char for hex */
                                for (j = 0; j < HEXDUMP_WIDTH; j++) {
                                    if (i + j < size) {
                                        rt_kprintf("%c", __is_print(data[i + j]) ? data[i + j] : '.');
                                    }
                                }
                                rt_kprintf("\n");
                            }
                            rt_kprintf("\n");
                        }
                        rt_free(data);
                    } else {
                        rt_kprintf("Low memory!\n");
                    }
                }
            } else if (!rt_strcmp(operator, "write")) {
                if (argc < 4) {
                    rt_kprintf("Usage: %s.\n", sf_help_info[CMD_WRITE_INDEX]);
                    return;
                } else {
                    addr = strtol(argv[2], NULL, 0);
                    size = argc - 3;
                    uint8_t *data = rt_malloc(size);
                    if (data) {
                        for (i = 0; i < size; i++) {
                            data[i] = strtol(argv[3 + i], NULL, 0);
                        }
                        result = sfud_write(sfud_dev, addr, size, data);
                        if (result == SFUD_SUCCESS) {
                            rt_kprintf("Write the %s flash data success. Start from 0x%08X, size is %ld.\n",
                                    sfud_dev->name, addr, size);
                            rt_kprintf("Write data: ");
                            for (i = 0; i < size; i++) {
                                rt_kprintf("%d ", data[i]);
                            }
                            rt_kprintf(".\n");
                        }
                        rt_free(data);
                    } else {
                        rt_kprintf("Low memory!\n");
                    }
                }
            } else if (!rt_strcmp(operator, "erase")) {
                if (argc < 4) {
                    rt_kprintf("Usage: %s.\n", sf_help_info[CMD_ERASE_INDEX]);
                    return;
                } else {
                    addr = strtol(argv[2], NULL, 0);
                    size = strtol(argv[3], NULL, 0);
                    result = sfud_erase(sfud_dev, addr, size);
                    if (result == SFUD_SUCCESS) {
                        rt_kprintf("Erase the %s flash data success. Start from 0x%08X, size is %ld.\n", sfud_dev->name,
                                addr, size);
                    }
                }
            } else if (!rt_strcmp(operator, "status")) {
                if (argc < 3) {
                    uint8_t status;
                    result = sfud_read_status(sfud_dev, &status);
                    if (result == SFUD_SUCCESS) {
                        rt_kprintf("The %s flash status register current value is 0x%02X.\n", sfud_dev->name, status);
                    }
                } else if (argc == 4) {
                    bool is_volatile = strtol(argv[2], NULL, 0);
                    uint8_t status = strtol(argv[3], NULL, 0);
                    result = sfud_write_status(sfud_dev, is_volatile, status);
                    if (result == SFUD_SUCCESS) {
                        rt_kprintf("Write the %s flash status register to 0x%02X success.\n", sfud_dev->name, status);
                    }
                } else {
                    rt_kprintf("Usage: %s.\n", sf_help_info[CMD_RW_STATUS_INDEX]);
                    return;
                }
            } else if (!rt_strcmp(operator, "bench")) {
                if ((argc > 2 && rt_strcmp(argv[2], "yes")) || argc < 3) {
                    rt_kprintf("DANGER: It will erase full chip! Please run 'sf bench yes'.\n");
                    return;
                }
                /* full chip benchmark test */
                addr = 0;
                size = sfud_dev->chip.capacity;
                uint32_t start_time, time_cast;
                size_t write_size = SFUD_WRITE_MAX_PAGE_SIZE, read_size = SFUD_WRITE_MAX_PAGE_SIZE;
                uint8_t *write_data = rt_malloc(write_size), *read_data = rt_malloc(read_size);

                if (write_data && read_data) {
                    rt_memset(write_data, 0x55, write_size);
                    /* benchmark testing */
                    rt_kprintf("Erasing the %s %ld bytes data, waiting...\n", sfud_dev->name, size);
                    start_time = rt_tick_get();
                    result = sfud_erase(sfud_dev, addr, size);
                    if (result == SFUD_SUCCESS) {
                        time_cast = rt_tick_get() - start_time;
                        rt_kprintf("Erase benchmark success, total time: %d.%03dS.\n", time_cast / RT_TICK_PER_SECOND,
                                time_cast % RT_TICK_PER_SECOND / ((RT_TICK_PER_SECOND * 1 + 999) / 1000));
                    } else {
                        rt_kprintf("Erase benchmark has an error. Error code: %d.\n", result);
                    }
                    /* write test */
                    rt_kprintf("Writing the %s %ld bytes data, waiting...\n", sfud_dev->name, size);
                    start_time = rt_tick_get();
                    for (i = 0; i < size; i += write_size) {
                        result = sfud_write(sfud_dev, addr + i, write_size, write_data);
                        if (result != SFUD_SUCCESS) {
                            rt_kprintf("Writing %s failed, already wr for %lu bytes, write %d each time\n", sfud_dev->name, i, write_size);
                            break;
                        }
                    }
                    if (result == SFUD_SUCCESS) {
                        time_cast = rt_tick_get() - start_time;
                        rt_kprintf("Write benchmark success, total time: %d.%03dS.\n", time_cast / RT_TICK_PER_SECOND,
                                time_cast % RT_TICK_PER_SECOND / ((RT_TICK_PER_SECOND * 1 + 999) / 1000));
                    } else {
                        rt_kprintf("Write benchmark has an error. Error code: %d.\n", result);
                    }
                    /* read test */
                    rt_kprintf("Reading the %s %ld bytes data, waiting...\n", sfud_dev->name, size);
                    start_time = rt_tick_get();
                    for (i = 0; i < size; i += read_size) {
                        if (i + read_size <= size) {
                            result = sfud_read(sfud_dev, addr + i, read_size, read_data);
                        } else {
                            result = sfud_read(sfud_dev, addr + i, size - i, read_data);
                        }
                        /* data check */
                        if (memcmp(write_data, read_data, read_size))
                        {
                            rt_kprintf("Data check ERROR! Please check you flash by other command.\n");
                            result = SFUD_ERR_READ;
                        }

                        if (result != SFUD_SUCCESS) {
                            rt_kprintf("Read %s failed, already rd for %lu bytes, read %d each time\n", sfud_dev->name, i, read_size);
                            break;
                        }
                    }
                    if (result == SFUD_SUCCESS) {
                        time_cast = rt_tick_get() - start_time;
                        rt_kprintf("Read benchmark success, total time: %d.%03dS.\n", time_cast / RT_TICK_PER_SECOND,
                                time_cast % RT_TICK_PER_SECOND / ((RT_TICK_PER_SECOND * 1 + 999) / 1000));
                    } else {
                        rt_kprintf("Read benchmark has an error. Error code: %d.\n", result);
                    }
                } else {
                    rt_kprintf("Low memory!\n");
                }
                rt_free(write_data);
                rt_free(read_data);
            } else {
                rt_kprintf("Usage:\n");
                for (i = 0; i < sizeof(sf_help_info) / sizeof(char*); i++) {
                    rt_kprintf("%s\n", sf_help_info[i]);
                }
                rt_kprintf("\n");
                return;
            }
            if (result != SFUD_SUCCESS) {
                rt_kprintf("This flash operate has an error. Error code: %d.\n", result);
            }
        }
    }
}
MSH_CMD_EXPORT(sf, SPI Flash operate.);
#endif /* defined(RT_USING_FINSH) && defined(FINSH_USING_MSH) */
