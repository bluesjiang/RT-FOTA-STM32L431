#include "usart.h"

#include <string.h>
#include <ring.h>

#ifdef BSP_USING_UART1
    extern UART_HandleTypeDef huart1;
	#define HUART huart1
	#define USART USART1
	extern void MX_USART1_UART_Init();
	#define MX_UART_UART_Init() MX_USART1_UART_Init()		
#endif
#ifdef BSP_USING_UART2
    extern UART_HandleTypeDef huart2;
	#define HUART huart2
	#define USART USART2	
	extern void MX_USART2_UART_Init();
	#define MX_UART_UART_Init() MX_USART2_UART_Init()		
#endif
#ifdef BSP_USING_UART3
    extern UART_HandleTypeDef huart3;
	#define HUART huart3
	#define USART USART3	
	extern  void MX_USART3_UART_Init();
	#define MX_UART_UART_Init() MX_USART3_UART_Init()		
#endif
#ifdef BSP_USING_UART4
    extern HUART huart4;
	#define UART huart4
	#define USART USART4	
	extern  void MX_USART4_UART_Init();
	#define MX_UART_UART_Init() MX_USART4_UART_Init()		
#endif
#ifdef BSP_USING_UART5
    extern UART_HandleTypeDef huart5;
	#define HUART huart5
	#define USART USART5	
	extern  void MX_USART5_UART_Init();
	#define MX_UART_UART_Init() MX_USART5_UART_Init()		
#endif
#ifdef BSP_USING_UART6
    extern UART_HandleTypeDef huart6;
	#define HUART huart6
	#define USART USART6	
	extern  void MX_USART6_UART_Init();
	#define MX_UART_UART_Init() MX_USART6_UART_Init()	
#endif
#ifdef BSP_USING_LPUART1
    extern UART_HandleTypeDef hlpuart1;
	#define HUART hlpuart1
	#define USART LPUART1
	extern  void MX_LPUART1_UART_Init();
	#define MX_UART_UART_Init() MX_LPUART1_UART_Init()
#endif


uint8_t RxBuffer1[1];		// 用来接收串口1发送的数据
#define UART_RX_BUF_LEN 16
static rt_uint8_t uart_rx_buf[UART_RX_BUF_LEN] = {0};
static struct rt_ringbuffer  uart_rxcb;         /* 定义一个 ringbuffer cb */
static UART_HandleTypeDef UartHandle;
static struct rt_semaphore shell_rx_sem; /* 定义一个静态信号量 */


/* 移植控制台，实现控制台输出, 对接 rt_hw_console_output */
void rt_hw_console_output(const char *str)
{
    rt_size_t i = 0, size = 0;
    char a = '\r';

    __HAL_UNLOCK(&HUART);

    size = rt_strlen(str);
    for (i = 0; i < size; i++)
    {
        if (*(str + i) == '\n')
        {
            HAL_UART_Transmit(&HUART, (uint8_t *)&a, 1, 1);
        }
        HAL_UART_Transmit(&HUART, (uint8_t *)(str + i), 1, 1);
    }
}
/* 移植 FinSH，实现命令行交互, 需要添加 FinSH 源码，然后再对接 rt_hw_console_getchar */
/* 中断方式 */
char rt_hw_console_getchar(void)
{
    char ch = 0;

    /* 从 ringbuffer 中拿出数据 */
    while (rt_ringbuffer_getchar(&uart_rxcb, (rt_uint8_t *)&ch) != 1)
    {
        rt_sem_take(&shell_rx_sem, RT_WAITING_FOREVER);
    } 
    return ch;   
}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == USART)	// 判断是由哪个串口触发的中断
	{
		rt_interrupt_enter();          //在中断中一定要调用这对函数，进入中断
		/* 读取到数据，将数据存入 ringbuffer */
        rt_ringbuffer_putchar(&uart_rxcb, RxBuffer1[0]);
		rt_sem_release(&shell_rx_sem);
		HAL_UART_Receive_IT(&HUART,RxBuffer1,1);		// 重新使能串口1接收中断
		rt_interrupt_leave();    //在中断中一定要调用这对函数，离开中断
	}
}

int uart_init(void)
{
	/* 初始化串口接收 ringbuffer  */
    rt_ringbuffer_init(&uart_rxcb, uart_rx_buf, UART_RX_BUF_LEN);

    /* 初始化串口接收数据的信号量 */
    rt_sem_init(&(shell_rx_sem), "shell_rx", 0, 0);
	MX_UART_UART_Init();
	HAL_UART_Receive_IT(&HUART,RxBuffer1,1);			// Enable the USART1 Interrupt
    return 0;
}
int uart_deinit(void)
{
    /* 初始化串口接收数据的信号量 */
    rt_sem_delete(&(shell_rx_sem));
	HAL_UART_MspDeInit(&HUART);
    return 0;
}

UART_HandleTypeDef *rt_console_get_device(void)
{
    return &HUART;
}
